import time
from plyer import notification as nn

if __name__ == "__main__":
    while True:
        nn.notify(
            title = "** PLaese Drink Water Now !!",
            message="The National Academies of Sciences, Engineering, and Medicine determined that an adequate daily fluid intake is: About 15.5 cups (3.7 liters) of fluids for men. About 11.5 cups (2.7 liters) of fluids a day for women.",
            app_icon="./water.ico",
            timeout= 10
            )
            time.sleep(60*60)
